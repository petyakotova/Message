﻿using System;

public class BirthdayWisher
{
    public BirthdayWisher(string personName, DateTime birthDate)
    {
        this.PersonName = personName;
        this.BirthDate = birthDate;
    }
    public readonly DateTime BirthDate;
    public readonly string PersonName;

    const string BirthdayMessage =
        "Dear {0}! Wishing you a happy birthday for {1}. Congratulations on being {2} years old!";

    public void WishBirthday()
    {
        Console.WriteLine(String.Format(BirthdayMessage, PersonName, BirthDate.ToString("MMMM dd"), DateTime.Today.Year - BirthDate.Year));
    }
}

public class SecretMessage
{
    public static void Main(string[] args)
    {
        BirthdayWisher naomi = new BirthdayWisher("Ilian", new DateTime(1997, 08, 02));
        naomi.WishBirthday();

        Console.WriteLine("Press any key to end.");
        Console.ReadKey();
    }
}